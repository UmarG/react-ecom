import PleaseSignIn from '../components/PleaseSignin';
import Permissions from '../components/Permissions';

const PermissionsPage = () => {
  return (
    <PleaseSignIn>
      <Permissions />
    </PleaseSignIn>
  );
};

export default PermissionsPage;
