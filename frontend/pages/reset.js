import ResetForm from '../components/Reset';

const Reset = props => {
  return (
    <>
      <p>Reset your password</p>
      <ResetForm resetToken={props.query.resetToken} />
    </>
  );
};

export default Reset;
